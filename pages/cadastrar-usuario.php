<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include '../components/html-head.php' ?>

    <title>Cadastrar Usuário</title>
</head>
<body class="body-background">
    <section class="content">
        <div class="register-content">

            <h1 class="register-title">Coisas Emprestadas</h1>

            <p class="form-title">CADASTRAR USUÁRIO</p>

            <form class ="register-form" action="../utils/register-user.php" method="post">

                <?php

                const CLASS_INPUT_CONTAINER = "register-input-container";
                const CLASS_INPUT_LABEL = "register-input-label";
                const CLASS_INPUT = "register-input";
                $inputs = array(
                    array("label" => "nome completo", "id" => "name", "type" => "text"),
                    array("label" => "email", "id" => "email", "type" => "email"),
                    array("label" => "senha", "id" => "pwd", "type" => "password"),
                    );

                foreach ($inputs as $input){
                    echo
                    "<div class=" . CLASS_INPUT_CONTAINER . ">
                        <label class=" . CLASS_INPUT_LABEL . " for={$input['id']}>{$input['label']}:</label>
                        <input class=" . CLASS_INPUT . " type={$input['type']} id={$input['id']} name={$input['id']} autocomplete='off' required>
                    </div>";
                };

                ?>

                <input class="submit-button" type="submit" value="Cadastrar Usuário">
            </form>
            <p class="back-home">
                <a href="../" class="back-home-link">Voltar à página inicial.</a>
            </p> 
        </div>
    </section>

    <?php
        include '../components/footer.php'
    ?>
</body>
</html>