<?php
    require "../utils/auth.php";
    include "../utils/db-connect.php";
    include "../utils/date-format.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include "../components/html-head.php"; ?>
    
    <title>Listar Itens</title>
</head>
<body class="logged-page">
    <?php include "../components/nav-menu.php"; echo (strtotime(date('Y-m-d')) > strtotime('2021-11-29'));?>

    <section class="list-items">
        <h1 class="page-title">Lista de Itens</h1>
        
        <table class="table-list">
            <thead class="table-header">
                <tr>
                    <th class="table-header-item">id</th>
                    <th class="table-header-item">item</th>
                    <th class="table-header-item">data de empréstimo</th>
                    <th class="table-header-item">data para devolver</th>
                    <th class="table-header-item">emprestado para</th>
                    <th class="table-header-item">contato</th>
                    <th class="table-header-item">atrasado</th>
                    <th class="table-header-item">disponível</th>
                    <th class="table-header-item"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                include '../components/delete-icon.php';

                    $sql = "SELECT 
                                *
                            FROM
                                items";

                    $res = mysqli_query($dbcon, $sql);
                    $rows = mysqli_fetch_all($res, MYSQLI_ASSOC);

                    foreach ($rows as $key => $row) { 
                        $today = date('Y-m-d');
                        $reordered_row = array (
                            $row["id"],
                            $row["name"],
                            !!$row["dtborrow"] ? dateFormat($row["dtborrow"]) : "-",
                            !!$row["dtreturn"] ? dateFormat($row["dtreturn"]) : "-",
                            $row["borrower"],
                            $row["borrower_contact"],
                            strtotime($today) > strtotime($row["dtreturn"]) && !!$row["borrowed"] ? "sim" : "-",
                            !$row["borrowed"] ? "sim" : "não",
                            delete_icon($row["id"]),
                    );
                    
                    echo "<tr class='" . ($key % 2 == 0 ? "even-row" : "odd-row") . ($reordered_row[6] == 'sim' ? " late-row" : "") ."'>";

                        foreach ($reordered_row as $item) {

                            echo '<td class="table-data-item">' . $item . "</td>";
                        };

                        echo "</tr>";
                    };
                ?>
            </tbody>
        </table>
    </section>

    <?php include "../components/footer.php"; ?>
</body>
</html>