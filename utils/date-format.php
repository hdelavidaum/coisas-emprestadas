<?php
    function dateFormat($date) {
        $date_object = date_create($date);
        return date_format($date_object, "d/m/Y");
    };
?>