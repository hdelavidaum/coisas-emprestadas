<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bungee+Inline&family=Lato:wght@400;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">


    <title>Coisas Emprestadas</title>

</head>
<body class="body-background">
    <section class="content" >
        <div class="login-content">

        <h1 class="login-title">Coisas Emprestadas</h1>

        <p class="form-title">LOGIN</p>

        <form class="login-form" action="./utils/login.php" method="post" enctype="multipart/form-data">

            <div class="login-input-container">
                <label class="login-input-label" for="email">E-mail:</label>
                <input class="login-input" type="email" id="email" placeholder="digite aqui seu e-mail de login" name="email" autocomplete="off" required autofocus>
            </div>

            <div class="login-input-container">
                <label class="login-input-label" for="pwd">Senha:</label>
                <input class="login-input" type="password" id="password" placeholder="digite aqui sua senha" name="pwd" required>
            </div>

            <input class="submit-button" type="submit" value="Entrar">
            </form>

            <p class="register">
            Não tem conta? Registre-se <a href="./pages/cadastrar-usuario.php" class="register-link">aqui</a>.
            </p> 
        </div>
    </section>
    <?php
        include './components/footer.php';
    ?>
</body>


</html>