<?php
    include "../utils/auth.php"
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include "../components/html-head.php"; ?>

    <title>Registrar Empréstimo</title>
</head>
<body class="logged-page">
    <?php include "../components/nav-menu.php"; ?>

    <section class="logged-form">

    <p class="form-title">REGISTRAR EMPRÉSTIMO</p>

            <form class ="logged-form" action="../utils/register-borrowing.php" method="post">

                <?php

                const CLASS_INPUT_CONTAINER = "logged-input-container";
                const CLASS_INPUT_LABEL = "logged-input-label";
                const CLASS_INPUT = "logged-input";
                $inputs = array(
                    array("label" => "item", "id" => "item", "type" => "text"),
                    array("label" => "emprestado por", "id" => "borrower", "type" => "text"),
                    array("label" => "contato de quem emprestou", "id" => "contact", "type" => "text"),
                    array("label" => "data do empréstimo", "id" => "borrow-date", "type" => "date"),
                    array("label" => "data para devolução", "id" => "return-date", "type" => "date"),
                    );

                foreach ($inputs as $input){
                    echo
                    "<div class=" . CLASS_INPUT_CONTAINER . ">
                        <label class=" . CLASS_INPUT_LABEL . " for={$input['id']}>{$input['label']}:</label>
                        <input class=" . CLASS_INPUT . " type={$input['type']} id={$input['id']} name={$input['id']} required >
                    </div>";
                };

                ?>

                <input class="submit-button" type="submit" value="Registrar empréstimo">
            </form>

    </section>

    <?php include "../components/footer.php"; ?>
</body>
</html>