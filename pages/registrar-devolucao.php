<?php
    require "../utils/auth.php";
    include "../utils/db-connect.php";

    $sql = "SELECT
                *
            FROM
                items
            WHERE
                borrowed = 1";

    $res = mysqli_query($dbcon, $sql);
    $rows = mysqli_fetch_all($res, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include "../components/html-head.php"; ?>

    <title>Registrar Devolução</title>
</head>
<body class="logged-page">
    <?php include "../components/nav-menu.php"; ?>

    <section class="logged-form">

    <p class="form-title">REGISTRAR DEVOLUÇÃO</p>

            <form class ="logged-form" action="../utils/to-return.php" method="post">

                <select class="return-select" name="to_return">
                    <?php
                        foreach ($rows as $row) {
                            $item = array("id" => $row["id"], "name" => $row["name"]);
                            
                            echo "<option class='return-option' value={$item['id']}>{$item['name']}</option>";
                        };
                    ?>
                </select>

                <input class="submit-button" type="submit" value="Devolver">
            </form>

    </section>
    
    <?php include "../components/footer.php"; ?>
</body>
</html>