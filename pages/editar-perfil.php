<?php
    require "../utils/auth.php";
    include "../utils/db-connect.php";

    $user_id = $_GET["userId"];

    $sql = "SELECT
                *
            FROM
                users
            WHERE
                id = $user_id";

    $res = mysqli_query($dbcon, $sql);
    $row = mysqli_fetch_assoc($res);

    $name = $row["name"];
    $email = $row["email"];
    $pwd = $row["pwd"];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php include "../components/html-head.php"; ?>

    <title>Editar Perfil</title>
</head>
<body class="logged-page">
    <?php include "../components/nav-menu.php"; ?>
    
    <section class="logged-form">

    <p class="form-title">EDITAR PERFIL</p>

            <form class ="logged-form" action="../utils/user-update.php" method="post">

                <?php

                const CLASS_INPUT_CONTAINER = "logged-input-container";
                const CLASS_INPUT_LABEL = "logged-input-label";
                const CLASS_INPUT = "logged-input";
                $inputs = array(
                    array("label" => "", "id" => "id", "type" => "hidden", "value" => $user_id),
                    array("label" => "nome completo", "id" => "name", "type" => "text", "value" => $name),
                    array("label" => "email", "id" => "email", "type" => "email", "value" => $email),
                    array("label" => "senha", "id" => "pwd", "type" => "password", "value" => $pwd),
                    );

                foreach ($inputs as $input){
                    switch ($input["type"]) {
                        case 'hidden':
                            echo "<input type={$input['type']} name={$input['id']} value={$input['value']} />";
                            break;
                        
                        default:
                            echo "<div class=" . CLASS_INPUT_CONTAINER . ">
                                <label class=" . CLASS_INPUT_LABEL . " for={$input['id']}>{$input['label']}:</label>
                                <input class=" . CLASS_INPUT . " type={$input['type']} id={$input['id']} name={$input['id']} value='{$input['value']}' >
                            </div>";
                            break;
                    };
                };

                ?>

                <input class="submit-button" type="submit" value="Salvar">
            </form>

    </section>

    <?php include "../components/footer.php"; ?>
</body>
</html>