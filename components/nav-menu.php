<nav class="side-menu">
    <p class="menu-title">Coisas Emprestadas</p>
    <ul class="menu-list">

    <?php

        $menu_items = array(
            array("href" => "./listar-coisas.php", "label" => "Listar coisas", "filename" => "listar-coisas"),
            array("href" => "./registrar-emprestimo.php", "label" => "Registrar empréstimo", "filename" => "registrar-emprestimo"),
            array("href" => "./registrar-devolucao.php", "label" => "Registrar devolução", "filename" => "registrar-devolucao"),
            array("href" => "./editar-perfil.php?userId={$_SESSION['id']}", "label" => "Editar perfil", "filename" => "editar-perfil"),
        );

        $script_name = $_SERVER["SCRIPT_NAME"];
        
        foreach ($menu_items as $item) {
            $menu_class_css = "menu-link ";
            $menu_class_active_css = "menu-active ";

            if (str_contains($_SERVER['SCRIPT_NAME'], $item["filename"]) > 0) {
                $menu_class_css .= $menu_class_active_css;
            };

            echo "<li class='{$menu_class_css}'>
                    <a href={$item['href']}>{$item['label']}</a>
                </li>";
        }

        ?>
    </ul>
    <div class="menu-logout"><a href="../utils/logout.php">SAIR</a></div>
</nav>